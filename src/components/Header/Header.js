import React from 'react'

const Header = ()=>{
    return(
    		<div className="text-center header">
    			<div id="text-header" >Todo List</div>
    		</div>
    	);
};

export default Header;
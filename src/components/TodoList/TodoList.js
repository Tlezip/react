import React,{Component} from 'react';

import TodoItem from './TodoItem';

class TodoList extends Component{
    constructor(props){
        super(props);
        
        this.state = {
            todoList: [],
            todoDT: [],
            todoTime: []
        };

        this.renderListItem = this.renderListItem.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        let nd = new Date();
        let time = nd.toString();
        let todoList = this.state.todoList;
        let todoDT = this.state.todoDT;
        let todoTime = this.state.todoTime;
        todoList.push(nextProps.task);
        todoDT.push(nextProps.DT);
        todoTime.push(time);
        this.setState({todoList: this.state.todoList});
        this.setState({todoDT: this.state.todoDT});
        this.setState({todoTime: this.state.todoTime});
    }

    renderListItem(){

        return this.state.todoList.map((list,i)=>{
                
            return(

                

                    <TodoItem task={list} index={i} DT={this.state.todoDT[i]} time={this.state.todoTime[i]} onDel={(index) => {this.onDel(i)}}/>
                
            )
        })
    }
    
    onDel(index){
        console.log(index);
        let temp = this.state.todoList;
        let temp2 = this.state.todoDT;
        let temp3 = this.state.todoTime;
        temp.splice(index,1);
        temp2.splice(index,1);
        temp3.splice(index,1);
        this.setState({todoList: temp});
        this.setState({todoDT: temp2});
        this.setState({todoTime: temp3});
    }

    render(){
        return(
            <div>
                <ul>
                    {this.renderListItem()}
                </ul>
            </div>        
        )
    }
}

export default TodoList;
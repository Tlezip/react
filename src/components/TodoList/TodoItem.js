import React,{Component} from 'react';

import TodoDetail from './TodoDetail';

class TodoItem extends Component{
    constructor(props){
        super(props);

        this.onClickDel = this.onClickDel.bind(this);
        this.showTime = this.showTime.bind(this);
        this.showDetail = this.showDetail.bind(this);
    }
    
    onClickDel(){
        this.props.onDel(this.props.index.value);
    }

    showTime(){
                let nd = new Date();
                let time = nd.toString();

                return(time);
    }

    showDetail(){
        
    }

    render(){

        return(
            <div>
                <div className="panel panel-default box-detail" style={{textAlign: 'left'}}>
                    <div className="panel-heading">
                        <a data-toggle="collapse" data-parent="#accordion" href={`#${this.props.index}`}>{this.props.task}</a>
                        <div style={{float: 'right'}} ><button onClick={this.onClickDel} className="btn-circle-sm">X</button></div>
                    </div>
                    <div id={this.props.index} className="panel-collapse collapse">
                        <div className="panel-body" >{this.props.DT}</div>
                        <div style={{textAlign: 'right'}}>{this.props.time}</div>
                    </div>
                </div>
            </div>

        );
    }
}

export default TodoItem;
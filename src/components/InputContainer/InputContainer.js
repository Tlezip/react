import React,{Component} from 'react';

class InputContainer extends Component{

    constructor(props){
        super(props);

        this.onClickAddTask = this.onClickAddTask.bind(this);
    }

    onClickAddTask(){
        this.props.onTaskAdd(this.refs.addTaskInput.value);
        this.props.onDTAdd(this.refs.addDTInput.value);
    }

    render(){
        return(
            <div >
                <div className="col-xs-2 mg-ip">
                <input ref="addTaskInput" className="form-control" placeholder="Add new task..."/>
                </div>

                <div className="col-xs-3 mg-ipbig">
                <textarea ref="addDTInput" className="form-control" placeholder="Enter Detail Here..."/>
                </div>
                <br/>
                <div className="mg-add">
                <button className="btn btn-default" onClick={this.onClickAddTask}>Add</button>
                </div>
            </div>
        );
    }
}

export default InputContainer;
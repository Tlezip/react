import React, {Component} from 'react';
import Header from './Header/Header';
import InputContainer from './InputContainer/InputContainer';
import TodoList from './TodoList/TodoList';

class App extends Component {
    constructor(props){
        super(props);

        this.state = {
            task: '',
            DT: ''
        };

        this.onTaskAdd = this.onTaskAdd.bind(this);
        this.onDTAdd = this.onDTAdd.bind(this);
    }

    onTaskAdd(value){
        this.setState({task: value});
    }

    onDTAdd(value2){
        this.setState({DT: value2});
    }

    render() {
        return (
            <div className="text-center">
                <Header/>
                <InputContainer onTaskAdd={(value)=>{this.onTaskAdd(value)}} onDTAdd={(value2)=>{this.onDTAdd(value2)}}/>
                <TodoList task={this.state.task} DT={this.state.DT} />
            </div>
        );
    }
}

export default App;